# Julia Vocabulary 

Collection of standard things I do in R and how to do it in Julia.

# Inspiration

This is very much inspired by 
[this notebook](http://www.johnmyleswhite.com/notebook/2012/04/09/comparing-julia-and-rs-vocabularies/) 
by [John Myles White](http://www.johnmyleswhite.com/) 
building on [Hadley Wickahm's](http://hadley.nz/) 
[R vocabulary](http://adv-r.had.co.nz/Vocabulary.html) 
from the first version of the [Advanced R](https://adv-r.hadley.nz/index.html) book.

# Resources

* [Julia Cheat-Sheet](https://juliadocs.github.io/Julia-Cheat-Sheet/)
* [Manual](https://docs.julialang.org/en/latest/)
* [Tom Kwong: Hands-On Design Patterns and Best Practices with Julia](https://subscription.packtpub.com/book/application_development/9781838648817)

# Working with DataFrames

## `transform()`, `paste()`

### R 

```{r}
transform(iris, Sepal.Area = Sepal.Length * Sepal.Width)
transform(iris, SrS = paste(Species, rev(Species), sep ="_"))
```
### Julia

* [DataFrames](https://juliadata.github.io/DataFrames.jl/stable/lib/functions/#DataFrames.transform)
* [DataFramesMeta](https://github.com/JuliaData/DataFramesMeta.jl#transform)

```{julia}
using RDatasets, DataFrames
iris = dataset("datasets", "iris")
transform(iris, [:SepalLength, :SepalWidth] => ((x,y) -> x .* y) => "SepalArea")
using DataFramesMeta
@transform(iris, SepalArea = :SepalLength .* :SepalWidth)
@transform(iris, SrS = join.(zip(:Species, reverse(:Species)), "_"))
Pkg.status("DataFrames") ## DataFrames v0.21.3
Pkg.status("DataFramesMeta") ## DataFramesMeta v0.5.1
```

## `rbind()`

### R

```{r}
L <- list(data.frame(a=1:3, b=4:6),data.frame(a=4:6, b=7:9))
rbind(L[[1]], L[[2]])
do.call(rbind, L)
plyr::ldply(L, I)
L <- list(data.frame(a=1:3, b=4:6),data.frame(a=4:6, c=7:9))
plyr::rbind.fill(L[[1]], L[[2]])
```

### Julia

* [vcat](https://docs.julialang.org/en/v1/base/arrays/#Base.cat)

```{julia}
using DataFrames
L = [DataFrame(a=1:3, b=4:6), DataFrame(a=4:6, b=7:9)]
vcat(L[1], L[2])
vcat(L...)
L = [DataFrame(a=1:3, b=4:6), DataFrame(a=4:6, c=7:9)]
vcat(L...; cols = :union)
```

## `subset()`, `%in%`

### R

```{r}
subset(iris, Species == "setosa" | Species == "virginica")
subset(iris, Species %in% c("setosa", "virginica"))
```

### Julia

* [DataFramesMeta](https://github.com/JuliaData/DataFramesMeta.jl#transform)
* [in, ∈](https://docs.julialang.org/en/v1/base/collections/#Base.in) 

```{julia}
using RDatasets, DataFrames
iris = dataset("datasets", "iris")
using DataFramesMeta
@where(iris, (:Species .== "setosa") .| (:Species .== "virginica"))
@where(iris, [x in ("setosa", "virginica") for x in :Species])
```

